<!-- 
  Thank you for reporting a possible bug in the community website.
  Please fill in as much of the template below as you can.
-->

<!-- e.g. Firefox 93.0 x64 -->
Browser: 
<!-- e.g. the output of `uname -a` -->
Operating System:

### Problem description
<!-- Also tell us, what did you expect to happen? -->

Steps to reproduce the behavior:
1. Goto '...'
2. Click on '...' 
3. See error '...'

### Relevant log output
<!-- If you are running the website locally, please paste the log output. -->


### Screenshots
<!-- If applicable, add screenshots to help explain your problem. -->


### I have

- [ ] read the `README.md`
- [ ] searched for similar issues.

### Code of Conduct
<!-- To submit this issue, you need to agree to follow our Code of Conduct. -->

- [ ] I agree to follow this project's [Code of Conduct](https://matrix-python.github.io/docs/contributing/code_of_conduct/)
