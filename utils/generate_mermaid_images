#!/bin/bash
# MIT License
#
# Copyright (c) 2022 Michael Sasser <Info@MichaelSasser.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Disable sandbox on ci by adding any argument.
[ -z "$1" ] || echo '{"args": ["--no-sandbox"]}' > puppeteer-config.json

for file in mermaid/*; do
    filename="$(basename "$file")"
    if [[ "${filename##*.}" == 'mmd' ]]; then
        if [ -f 'puppeteer-config.json' ]; then
          ./node_modules/.bin/mmdc -p puppeteer-config.json \
               -i "mermaid/${filename%.*}.mmd" \
               -o "static/images/mermaid/${filename%.*}_dark.svg" \
               -b transparent \
               -t dark \
               -q
          ./node_modules/.bin/mmdc -p puppeteer-config.json \
               -i "mermaid/${filename%.*}.mmd" \
               -o "static/images/mermaid/${filename%.*}.svg" \
               -b transparent \
               -q
        else
          ./node_modules/.bin/mmdc -i "mermaid/${filename%.*}.mmd" \
               -o "static/images/mermaid/${filename%.*}_dark.svg" \
               -b transparent \
               -t dark \
               -q
          ./node_modules/.bin/mmdc -i "mermaid/${filename%.*}.mmd" \
               -o "static/images/mermaid/${filename%.*}.svg" \
               -b transparent \
               -q
        fi
        echo "mmd    mermaid/$filename -> static/images/mermaid/${filename%.*}_dark.svg"
        echo "mmd    mermaid/$filename -> static/images/mermaid/${filename%.*}.svg"
    fi
done
