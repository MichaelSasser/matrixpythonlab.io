---
title: "Helpdesk"
description: "Member of the moderation team"
lead: "Hi, I'm helping you to get in touch with the Moderators."
date: 2021-04-30T01:33:00+02:00
lastmod: 2021-02-16T01:33:00+00:00
draft: false
images: ["helpdesk_ava.svg"]
avatar: "helpdesk_ava.svg"
matrix_moderator: false
bot: true
---

<div style = "text-align: left">

{{< alert preset="todo" >}}{{< /alert >}}

## Using the Helpdesk Bot

Just write a <abbr title="Direct Message">DM</abbr> to the bot. The moderators
will be notified and can respond to your issue. Your messages will be
end-to-end encrypted and are not visible to anyone other
than you and the moderators.

## Reporting Issues

If you experience an issue with our services, or you encounter any problem,
that can be discussed in public, use our Meta Room
{{< identifier "#python-meta:matrix.org" >}}.

{{< reporting_issues >}}
