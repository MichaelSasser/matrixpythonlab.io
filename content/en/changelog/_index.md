---
title: Community "Changelog"
description:
  "A brief glimpse at our history"
lead:
  "A brief glimpse at our history."
date: 2021-12-10T11:13:42+01:00
lastmod: 2021-12-10T11:13:42+01:00
draft: false
images: []
---

The Python community on Matrix evolved quite a lot over the years. From just a handful
to hundreds and thousands of users who are members of our rooms. The following timeline shows the milestones we reached so far.
