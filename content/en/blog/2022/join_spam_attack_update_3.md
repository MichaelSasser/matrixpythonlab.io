---
title: "Join Spam Attack Update 3"
description: ""
lead: "Apparently, we have been unblocked on matrix.org"
date: 2022-05-10T20:27:00+02:00
lastmod: 2022-05-10T20:27:00+02:00
draft: false
weight: 50
contributors: ["Michael Sasser"]
categories: ["Community", "Matrix"]
tags: ["Announcement"]
images: []
header_image: ""
floating_image: ""
floating_image_width: 40
floating_image_position: "right"
floating_image_caption: ""
---

If you still see a
different timeline, you can go to `All settings` -> `Help & About` and click
on `Clear cache and reload` (You will still be logged in afterwards). The
room is now public again. Thank you for your patience.
