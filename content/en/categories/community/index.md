---
title: "Community"
description: "Python Community on Matrix (Category)"
lead: "News about our Python Community on Matrix."
date: 2022-05-13T13:39:12+02:00
lastmod: 2022-05-13T13:39:12+02:00
draft: false
images: ["community-logo.svg"]
contributors: ["Michael Sasser"]
---
