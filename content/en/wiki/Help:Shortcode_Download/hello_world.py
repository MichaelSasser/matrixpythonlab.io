#!/usr/bin/env python3


def main() -> None:
    print("Hello from the Python Community on Matrix!")

if __name__ == "__main__":
    main()
