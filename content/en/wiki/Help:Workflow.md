---
title: "Help:Workflow"
url: "/wiki/Help:Workflow"
description: "Contribute to our communtiy by improving our website."
lead: "Contribute to our communtiy by improving our website."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute"]
draft: false
images: []
weight: 50
toc: true
---

{{< alert preset="coc" >}}{{< /alert >}}
{{< alert preset="contrib-legal" >}}{{< /alert >}}

You will find the code of this website on [GitLab](https://gitlab.com/matrixpython/matrixpython.gitlab.io).

{{< alert >}}
Before you start, make sure you hand in an
<a href="https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/issues/new">issue</a>.
Describe, what you like to change/add and why.
{{< /alert >}}

1. Make sure you have Node.js, `npm`, `git` and `git-lfs` installed.
2. Create a fork of this repository.
3. Clone the fork (as `origin`) to your local machine. Make sure the LFS
   content is pulled during that step. Alternatively, you can pull it
   afterward manually.
4. Add this repository as a remote named `upstream`.
5. Create a new branch from the
   [main](https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/tree/main)
   branch. Let's say your issue was issue `#42` and you want to create a
   feature. Your branch name would be `feature/#42-my-feature`.
6. Install the required tools with `npm install` and serve the website on your
   machine with `npm run dev`.
7. (Optional) Use `./utils/get_matrix_rooms <homeserver>`, e.g.
   `./utils/get_matrix_rooms matrix.org` to render the user counter and
   activate some features.
8. (Optional) Use `./utils/get_matrix_users <homeserver> <your matrix session token>`, e.g.
   `./utils/get_matrix_users matrix.org "asdfoobarbaz"` to render the matrix
   user pills with avatars and activate some features.
9. Open the URL `[http://localhost:1313](http://localhost:1313)` in your web
   browser. You should now see the website.
10. Implement the feature (or bugfix) you described in your issue. The website
    in your browser will update as soon as you save the file you are editing.
11. Commit and publish your branch to your fork (`origin`).
12. Create a `Pull Request` from the branch, which contains your changes to
    this repository's `master` branch.
13. Once the pull request is reviewed and merged, you can pull the changes from
    `upstream` (this repository) to your local repository and start over again
    from step 5. **Don't forget to create an issue first.**

We recommend to use the
[GLab - GitLab CLI tool](https://github.com/profclems/glab).

### About the Website

The repository has one branch with infinite lifetime:

- The
  [main](https://gitlab.com/matrixpython/matrixpython.gitlab.io/)
  branch -- the website reflects the current state of the main branch. When
  it gets changed, GitLab CI automatically builds and deploys the website.

When you are working with the repo, make sure to follow the
[GitLab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html).

The Website is built with [Hugo](https://gohugo.io/) and based on the lovely
template [Doks](https://getdoks.org/), by
[Henk "h-enk" Verlinde](https://github.com/h-enk). You will find useful
information you may need in their
[documentation](https://getdoks.org/docs/prologue/introduction/),
[code](https://github.com/h-enk/doks) or
[discussions](https://github.com/h-enk/doks/discussions) of the `Doks`
template. Please keep in mind, their discussions and issues are for the
template they made and not for our project.
