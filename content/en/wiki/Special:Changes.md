---
title: "Special:Changes"
description:
  "A list of recent changes"
lead: "A list of recent changes"
url: "/wiki/Special:Changes/"
date: 2022-06-06T09:40:00+20:00
lastmod: 2022-06-06T09:40:00+20:00
contributors: ["Michael Sasser"]
wiki_categories: ["special"]
draft: false
images: []
weight: 100
toc: true
menu:
  wiki:
    name: Changes
    parent: wiki_tools
    weight: 10
  wiki_categories:
    name: Changes
    parent: wiki_categories_tools
    weight: 10
---

{{< alert preset="todo" >}}{{< /alert >}}

## GitLab

 All changes made to this website including this wiki are done using `git` and [GitLab](https://gitlab.com/). In [our repository](https://gitlab.com/matrixpython/matrixpython.gitlab.io) you find all the changes. To make it easier to follow wiki specific changes, issues and merge requests are labeled {{< gitlab_label label="wiki::page" color="#6699cc" >}}. With the following links, you can directly jump to them:

[Merged Merge Requests →](https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/merge_requests?scope=all&state=merged&label_name[]=wiki%3A%3Apage)

[Open Merge Requests →](https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/merge_requests?scope=all&state=opened&label_name[]=wiki%3A%3Apage)

[Closed Merge Requests →](https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/merge_requests?scope=all&state=closed&label_name[]=wiki%3A%3Apage)

[All Merge Requests →](https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/merge_requests?scope=all&state=all&label_name[]=wiki%3A%3Apage)

[Board →](https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/boards/4396930?label_name[]=wiki%3A%3Apage)
