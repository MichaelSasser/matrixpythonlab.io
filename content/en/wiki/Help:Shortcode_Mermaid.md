---
title: "Help:Shortcode Mermaid"
url: "/wiki/Help:Shortcode_Mermaid"
description: ""
lead: "Display prerendered mermaid graphs."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Mermaid
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`mermaid`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display, the during CI/CD pre-rendered, mermaid graphs as SVG vector 
        images.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

This shortcode allows you to display pre-rendered mermaid graphs.
Because mermaid is not a small library, we use another approach.
The mermaid code is rendered during CI, so we don't need to provide the user
with the large client side application, mermaid brings with when rendering
on the client side. Because of this, we created a directory `mermaid` in the
root of the project repo. Create your mermaid source code in that directory
with the file extension `.mmd`. During CI this will be rendered into two
`SVG` files. One for light mode, and one for dark mode. This file can be
used later with the `mermaid` shortcode. The shortcode does not only
add the `SVG` file to the page. It also ensures, that at least the mermaid
source file exists. Otherwise, it would throw an error.

To render a mermaid graph, on your developer machine, you can use the command
`npm run build:mermaid`. The developer warning, placed in place by the
`mermaid` shortcode will vanish, and the graph will be shown for the
appropriate website theme.

{{< alert >}}
When adding changes for a commit, the compiled images are ignored.
They are build during CI/CD using GitLab Pipelines.
{{< /alert >}}

## Parameters

The `mermaid` shortcode has the following parameters:

| Parameter | Description                                        |
| --------- | -------------------------------------------------- |
| `[0]`     | The name of the mermaid file without the extension |

## Examples

Let's say we create a file `/mermaid/example.mmd` in the project root, which
looks like this:

{{% readfile path="/mermaid/example.mmd" %}}

To see the image in the development environment, we can now run
`npm run build:mermaid`. Instead of an warning, the image shows up, when using
it in Markdown with:

```md
{{</* mermaid "example" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< mermaid "example" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/mermaid.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/mermaid.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Mermaid Shortcode" path="/layouts/shortcodes/mermaid.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_mermaid.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_mermaid.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Mermaid Style" path="/assets/scss/components/_shortcode_mermaid.scss" >}}

### CI/CD Script

The following script is run during continuous integration and continuous
delivery to compile the graphs to `SVG` vector images. The vector images are
used by the mermaid shortcode to ensure they exist and display them on the
page.

#### Generate Mermaid Images

Defined in: `/utils/generate_mermaid_images`

{{< alert preset="license-code" >}}{{< /alert >}}

This code is used to render the `mermaid` source files to the `SVG`-pairs.

{{% readfile path="/utils/generate_mermaid_images" type="bash" %}}

##### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Generate Mermaid Images CI/CD Script" path="/utils/generate_mermaid_images" >}}
