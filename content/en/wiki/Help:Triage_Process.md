---
title: "Help:Triage Process"
url: "/wiki/Help:Triage_Process"
description: ""
lead: "How we handle issues and use GitLab labels."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

# infobox:
#   header: GitLab Labels
#   data:
#     - type: subheader
#       header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
#     - type: header
#       header: Description
#     - type: label
#       label: Type
#       value: HTML Shortcode
#     - type: label
#       label: Nested?
#       value: No
#     - type: label
#       label: Shortcode
#       value: "`gitlab_label`"
#     - type: label
#       label: Return Type
#       value: HTML
#     - type: fulldata
#       label: Short Description
#       value: |
#         Display value or value-pair badges, which look like GitLab lables.  
#     - type: header
#       header: Development
#     - type: label
#       label: Maintainer
#       value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

## Organizational Information

In GitLab, groups are used to manage one or more related projects at the same
time. These groups can also be further divided in subgroups to organize content
in a more granular way. Projects are the lowest Organizational unit.
We currently only use our main group for all of our projects.

## Triage Process

Since our projects are related to each other, we use GitLab Labels on a group
basis, for the triage process, which allows us to group issues and merge
requests together and work with them as a whole.

Those group labels are inherited from our projects. Other labels, which are
only useful for a specific project exist only on a project basis.
Below you find all of them, together with their description.

Our "label structure" and triage process is heavily inspired by the
[Matrix maintainers](https://github.com/matrix-org/synapse/issues/9460).

### Issues

An issue is considered triaged when it has the Type and a either Severity
label, for defects or the Type and Scope label, for enhancements.

A
[Milestone](https://docs.gitlab.com/ee/user/project/milestones/)
or
[Iteration](https://docs.gitlab.com/ee/user/group/iterations/)
is assigned to them separately and might change during the process to mark
their priority over time. They might change and are not fixed.

### Merge Requests

Merge Requests usually only get situational labels assigned.

## Group Labels

Group labels are used Globally in our Python Community on Matrix GitLab Group.

### Type

Every issue is assigned a type.

| Label                                          | Description                                               |
| ---------------------------------------------- | --------------------------------------------------------- |
| {{< gitlab_label label="Type::Enhancement" >}} | New features, functional changes or improvements          |
| {{< gitlab_label label="Type::Defect" >}}      | Bugs, vulnerabilities, rendering issues or other problems |
| {{< gitlab_label label="Type::Task" >}}        | Refactoring, enabling or disabling functionality          |
| {{< gitlab_label label="Type::Other" >}}       | Anything else                                             |

### Severity

All issues labeled
{{< gitlab_label label="Type::Defect" inline=true >}} must also have a
**Severity** label.

| Label                                            | Description                                                                     |
| ------------------------------------------------ | ------------------------------------------------------------------------------- |
| {{< gitlab_label label="Severity::Critical" >}}  | Prevents access to a part or the entirety of the product and affects many users |
| {{< gitlab_label label="Severity::Major" >}}     | Severely degrades major functionality or features                               |
| {{< gitlab_label label="Severity::Minor" >}}     | Impairs non-essential features                                                  |
| {{< gitlab_label label="Severity::Tolerable" >}} | Minor cosmetic issues with low impact to users                                  |

### Security

When a
{{< gitlab_label label="Type::Defect" inline=true >}} is security related
it will also be labeled with a **Security** label.

| Label                                                | Description             |
| ---------------------------------------------------- | ----------------------- |
| {{< gitlab_label label="Security::Vulnerability" >}} | Confirmed vulnerability |
| {{< gitlab_label label="Security::Potential" >}}     | Potential vulnerability |

### Exceptions

In some cases actions are blocked until something else is resolved. The
following labels are used situationally.

| Label                                                    | Description                                                                                  |
| -------------------------------------------------------- | -------------------------------------------------------------------------------------------- |
| {{< gitlab_label label="Exception::Needs Discussion" >}} | The moderation team needs to discuss this first                                              |
| {{< gitlab_label label="Exception::Needs Info" >}}       | Additional information from the reporter has been requested in order to continue             |
| {{< gitlab_label label="Exception::Release Blocker" >}}  | This issue must be resolved / this merge request must be merged before releasing the product |
| {{< gitlab_label label="Exception::Awaiting Changes" >}} | Changes to a merge request were requested and must be re-reviewed before it can be merged    |
| {{< gitlab_label label="Exception::Cannot Reproduce" >}} | Unable to reproduce the issue                                                                |
| {{< gitlab_label label="Exception::Help Needed" >}}      | We don't know how to fix this issue, and would be grateful for a contribution                |
| {{< gitlab_label label="Exception::Help Wanted" >}}      | We know how to fix this issue, and would be grateful for any contribution                    |
| {{< gitlab_label label="Exception::Upstream" >}}         | The problem originates from an upstream source, which must be fixed first                    |
| {{< gitlab_label label="Exception::Other" >}}            | This issue or merge request is blocked for any other reason                                  |

### Other

In some cases specific marks are used.

| Label                                         | Description                                                                               |
| --------------------------------------------- | ----------------------------------------------------------------------------------------- |
| {{< gitlab_label label="Good First Issue" >}} | Good for newcomers                                                                        |
| {{< gitlab_label label="Bot" >}}              | The issue or merge request was created by a bot                                           |
| {{< gitlab_label label="Dependency" >}}       | Add, update or remove dependencies                                                        |
| {{< gitlab_label label="Future Task" >}}      | Resubmission: Some parts need to be resolved in the future, which cannot be done just yet |

## Project Labels

Project labels are only used in a specific project and cannot be seen anythere
else in other groups or subgroups.

### Exceptions

In some cases actions are blocked until something else is resolved. The
following labels are used situationally.

| Label                                        | Description                                                                |
| -------------------------------------------- | -------------------------------------------------------------------------- |
| {{< gitlab_label label="Exception::Hugo" >}} | Waiting until an upstream issue is fixed or feature is implemented in Hugo |

### Scopes

Scopes are Project specific labels to denote the scope or type of an issue.
If applicable {{< gitlab_label label="Type::Enhancement" inline=true >}}
have an Scope too.

#### Website

| Label                                             | Description                         |
| ------------------------------------------------- | ----------------------------------- |
| {{< gitlab_label label="Scope::Wiki" >}}          | Wiki page changes                   |
| {{< gitlab_label label="Scope::Help" >}}          | Help page changes                   |
| {{< gitlab_label label="Scope::Docs" >}}          | Documentation content changes       |
| {{< gitlab_label label="Scope::Blog" >}}          | Blog post or Category changes       |
| {{< gitlab_label label="Scope::Other Content" >}} | Other, non-specific content changes |
| {{< gitlab_label label="Scope::Other" >}}         | Other, non-specific                 |

<!-- ### Other -->

## Maintainer Labels

As maintainer, it is sometimes useful to group specific content to have a
better overview. For that reason maintainers are encouraged to create their own
labels.

| Label                                                                             | Organizational Unit (OU) |
| --------------------------------------------------------------------------------- | ------------------------ |
| {{< gitlab_label label="G-LABEL" >}}, {{< gitlab_label label="G-SCOPE::LABEL" >}} | Group                    |
| {{< gitlab_label label="S-LABEL" >}}, {{< gitlab_label label="S-SCOPE::LABEL" >}} | Subgroup                 |
| {{< gitlab_label label="P-LABEL" >}}, {{< gitlab_label label="P-SCOPE::LABEL" >}} | Project                  |
