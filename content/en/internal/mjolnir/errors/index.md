---
title: "Errors"
description: "What errors to expect and how to handle them"
lead: "What errors to expect and how to handle them."
date: 2022-07-10T06:06:00+02:00
lastmod: 2022-07-10T06:06:00+02:00
draft: false
images: 
  - 504-gateway-timed-out.png
  - error-processing-your-command.png
  - no-settings-found.png
  - error-processing-event.md
menu:
  internal:
    parent: "mjolnir"
weight: 230
toc: true
---

{{< accordion html=false >}}

<!-- There was an error processing your command -->
{{< accordion_item name="Error: There was an error processing your command" >}}

{{< img src="error-processing-your-command.png" alt="there was an error processing your command sample image" caption="<center><em>Example</em></center>" figure_class="border-0" width="600">}}

If mjolnir responds with: "There was an error processing your command - see
console/log for details" and it still shows the reaction `In Progress 1`,
it usually means, there was an error during the
communication between synapse. This is often the case when an event
`org.matrix.dummy_event` is in the place of the actual event, mjolnir is
requesting. This happens, when users from different homeservers redact an
event at roughly the same time, and that time is less than the sync lag
between those servers. In this case, both homeservers accept the event, but
during server to server communication only one of the two events can happen.
Otherwise, this leads to an increase in forward extremities.
This dummy event might be in the database until synapse
restarts. Dummy events are hidden from clients during server-client
communication. If a client, such as mjolnir requests this event, synapse
will respond with an "404" error (not found).
This behavior can be observed in the access logs of the reverse proxy and
mjolnir's logs. Though, mjolnir might not log anything
when this happens. There isn't really any documentation about that behavior.

Another possibility is, that an event is not yet synced, and whatever the
command was, might just work a few seconds or a minute later without this
error. This might happen on high server load, due to deployments in progress or
maintenance procedures.

In any case, please try not to restart mjolnir.
{{< /accordion_item >}}

<!-- 504 Gateway Time-out -->
{{< accordion_item name="Error: 504 Gateway Time-out" >}}

{{< img src="504-gateway-timed-out.png" alt="504 gateway time-out sample image" caption="<center><em>Example</em></center>" figure_class="border-0" width="400">}}

As the error suggests, synapse isn't responding in time to the request.
This might happen on high server load, due to deployments in progress or
maintenance procedures. Just wait a while and try to run the command again.
Depending on the cause and command, this might take seconds or hours to
resolve.

In any case, please try not to restart mjolnir.
{{< /accordion_item >}}

<!-- No settings found -->
{{< accordion_item name="Error: No settings found" >}}

{{< img src="no-settings-found.png" alt="there was an error processing your command sample image" caption="<center><em>Example</em></center>" figure_class="border-0" width="600">}}

`!mjolnir config get <Protection>` doesn't seem to work. You can use the
devtools to find the state event, the settings are stored in, if it is not
the default value.
{{< /accordion_item >}}

<!-- There was an error processing an event through a protection - see log for details. Event:  -->
{{< accordion_item name="Error: There was an error processing an event through a protection" >}}

{{< img src="error-processing-event.png" alt="there was an error processing your command sample image" caption="<center><em>Example</em></center>" figure_class="border-0" width="600">}}

When mjolnir starts spamming
`⚠ | Banning @user:homeserver.tld in SomeRoom for flooding (n messages in the last minute)`
and immediately responds to that with
`There was an error processing an event through a protection - see log for details. Event: https://matrix.to/#/!iuyQXswfjgxQMZGrfQ:matrix.org/$SomeEventIdentifier`,
then
it was trying to ban a moderator for flooding. This might happen, because
the moderator was mass redacting without using mjolnir for that. The bot
does for whatever reason not differentiate between users and moderators.
Because the moderator has a higher
[power level]({{< relref "commands" >}}#power-level) than the bot (and this
is the reason for it), the bot is not able to ban the moderator.
Just ignore this error.

If you, on the other hand, got the error by running a command in the command
and control room, something might be wrong. I don't remember having
such a case yet, but it is feasible. In this case, please contact
[Michael]({{< ref "michael-sasser" >}}).
{{< /accordion_item >}}

<!-- The bot doesn't react -->
{{< accordion_item name="The bot doesn't react" >}}

It is currently not possible to check the bots health status. So it is possible
that the bot hangs up or errors out without us noticing. In the past, this
situation happened once, but was a "Michael-masked-the-bot-and-forgot-it"
error.

Otherwise, this happens, when restarting or doing maintenance on the bot,
synapse, one of it's workers or dependencies. Usually the maintenance is only
triggered manually, and the steps are done automatically, but in some cases
additional steps are performed, where only the database is running. Then, the
additional maintenance steps are done manually and it might take a while, until
the bot gets enabled again.

Please contact [Michael]({{< ref "michael-sasser" >}}) **before** restarting
the bot, if you find yourself in a situation like this. He might just work on
the database and forgot to mention it before. If he does not respond within a
couple of hours, feel free do restart the bot yourself.

When restarting the bot, always make sure to wait, until the bot sends those
four messages before sending a command:

```text
Mjolnir is starting up. Use !mjolnir to query status.
Checking permissions...
Syncing lists...
Startup complete. Now monitoring rooms.
```

{{< /accordion_item >}}

{{< /accordion >}}
