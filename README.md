# The Python Community on Matrix Website

We are the _Python Community_ on Matrix, a free and open network for secure,
decentralized communication.

**This website is currently under construction!**

## I found a bug

Please let us know, if you found an issue with our website. You can do this
by handing in a _Bug report_. <br />
If you have any suggestions, we would appreciate a _Feature Request_.

Hand in a
[Bug Report or Feature Request →](
https://gitlab.com/matrixpython/matrixpython.gitlab.io/-/issues/new)

## I have a question

Feel free to join our Meta room
[`#python-meta:matrix.org`](https://matrix.to/#/#python-meta:matrix.org) on
Matrix to ask any questions about the website or community.

## Contribute to the Website

**Every text or image you use must have been created or written by yourself!**

By creating a Pull Request, you agree that the image or text may be used
without any restriction for community projects under the direction of the
moderation team at any time.

Non-compliance may have legal consequences and can lead to immediate exclusion
from the community.

To contribute to this project, check our
[Contributer Documentation →](
https://matrixpython.gitlab.io/docs/contributing/workflow/)

## License

Designed by the Python Community on Matrix team with the help of our
[contributors](https://matrixpython.gitlab.io/contributors/) ❤️.<br />
Based on [Doks](https://getdoks.org/"), built with
[Hugo](https://gohugo.io/).<br />
Code licensed
[MIT](
https://gitlab.com/matrixpython/matrixpython.gitlab.io/blob/master/LICENSE.md),
docs
[CC BY 4.0](
https://gitlab.com/matrixpython/matrixpython.gitlab.io/blob/master/LICENSE_DOCS.md).


### License of This Repo's Logo

The license of this repository is in https://gitlab.com/matrixpython/designs/-/blob/main/gitlab_logos/website_logo
