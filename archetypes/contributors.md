---
title: "{{ replace .Name "-" " " | title }}"
description: ""
lead: ""
date: {{ .Date }}
lastmod: {{ .Date }}
draft: true
images: ["{{ .Name | urlize }}.jpg"]
avatar: ""
matrix_identifier: ""
matrix_username: ""
matrix_moderator: flase
website_content: false
website_developer: false
bot: false
email: ""
gitlab_username: ""
---
