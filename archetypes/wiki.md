---
title: "{{- replaceRE "_" "\\s" (.Page.File.TranslationBaseName) }}"
url: "/wiki/{{- replaceRE "\\s" "_" (.Page.File.TranslationBaseName) }}/"
description: ""
lead: ""
date: {{ .Date }}
lastmod: {{ .Date }}
contributors: []
draft: true
images: []
weight: 50
toc: true
---
