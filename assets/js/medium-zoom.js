/*

  Img Shortcode
  =============

  Contributors: Michael Sasser
  Maintainer: Michael Sasser
  License: MIT
  Version: 1
  Child Shortcodes: None
  Parent Shortcodes: None

  LAYOUT: /layouts/shortcodes/img.html
  STYLE:  /assets/scss/components/_shortcode_img.scss
  DOCS:   /content/<language>/wiki/Help:Shortcode_Img.md
  LIB:    medium-zoom
  SCRIPT: /assets/js/medium-zoom.js

*/

import mediumZoom from 'medium-zoom'


document.addEventListener('lazybeforeunveil', () => {
    mediumZoom('#zoom-default', {
        margin: 70,
        background: 'rgba(00, 00, 00, .3)',
        scrollOffset: 0,
    })
})
